import axios from "axios"

export default class ApiRequest {
    static async getData(text) {
        let error = null

        try {
            const response = await axios.get(`https://corona.lmao.ninja/v2/countries/${text}`, {
                params: {
                    yesterday: false,
                    strict: false
                }
            })
            return [ response.data, error ]
        } catch (e) { 
            error = e.response.data.message
        }
        return [ null, error ]
    }
}