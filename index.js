import express from 'express'
import fs from 'fs/promises'
import path, { dirname } from 'path'
import { fileURLToPath } from 'url'
// import { body, validationResult } from 'express-validator'
import ApiRequest from './API/index.js'
import { format } from 'date-fns'
import { countriesList } from './data/countries.js'

const app = express()

const PORT = process.env.PORT || 3000

app.set('view engine', 'pug')

app.use(express.static('public'))
app.use(express.urlencoded({ extended: true }))

app.locals.dateFnsFormat = format

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)

const logsPath = path.resolve(__dirname, 'data', 'coronavirus.json')

app.get('/', async (req, res) => {
    const data = await fs.readFile(logsPath)
    const dataParsed = JSON.parse(data)
    const dataLength = dataParsed.length
    res.render(
        'index', 
        { data : dataLength ? dataParsed[dataParsed.length - 1] : dataParsed }
    )
})

app.post('/', async (req, res) => {
    const randomValue = getRandom(1, countriesList.length)
    const country = countriesList[randomValue]
    const [ data, error ] = await ApiRequest.getData(country) // обработка ошибок

    if (data) {
        await addDataToJSON(data)
    }

    res.redirect('/')
})

app.listen(PORT, () => {
    console.log(`Server is working on port ${PORT}`);
})




const addDataToJSON = async (data) => {
    const existingData = await fs.readFile(logsPath)
    const json = JSON.parse(existingData)
    json.push(data)
    await fs.writeFile(logsPath, JSON.stringify(json))
}

const getRandom = (min, max) => Math.floor(Math.random() * (max-min) + min)