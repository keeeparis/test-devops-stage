# DevOps Test

To run
```
docker build -t <name> .
docker run -dp 3000:3000 <name>
```
or 
```
docker-compose up -d
```

To exit
```
docker rm -f <container-id>
```

Data from requests is stored in 'data' folder in .json format.

Tests are in 'spec' folder -> <code>npm run test</code>

After pushing commit to Gitlab, docker creates an image of application, run tests and push to Dockerhub Registry.