import ApiRequest from "../API";

const sampleData = [
    { country: 'Kazakhstan', data: { country: 'Kazakhstan', cases: 1 } },
    { country: 'zakhstan', data: { country: 'Kazakhstan', cases: 1 } },
    { country: 'Russia', data: { country: 'Russia', cases: 1 } },
    { country: 'USA', data: { country: 'USA', cases: 1 } },
]

describe('Testing Api Requests', () => {
    sampleData.forEach(element => {
        test(`Testing ${element.country}`, async () => {
            const [ data, error ] = await ApiRequest.getData(element.country)
            expect( data ).not.toBeNull()
            expect( data.country ).toEqual(element.data.country)
            expect( data.cases ).toBeGreaterThan(element.data.cases)
            expect( error ).toBeNull()
        })
    })
    test('Testing Error request', async () => {
        const [ data, error ] = await ApiRequest.getData('hhh')
        expect( data ).toBeNull()
        expect( error ).toBe('Country not found or doesn\'t have any cases')
    })
})

