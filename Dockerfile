FROM node:17-alpine as base
WORKDIR /app
COPY package.json .
COPY package-lock.json .

FROM base as test
RUN npm install
COPY . .
RUN npm run test

ENV PORT 3000
VOLUME [ "/app" ]
CMD ["node", "index.js"]